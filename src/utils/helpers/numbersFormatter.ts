const maxFloatDigits = 6;
const priceImpactFloatDigits = 2;
const balanceFloatDigits = 2;

const toFixed = ({
  number,
  fixed,
  swing,
}: {
  number?: number | string;
  fixed?: number;
  swing?: boolean;
}) => {
  if (!number) return fixed ? (0).toFixed(fixed) : '0';

  let numberSrt = typeof number === 'number' ? number.toString() : number;

  const isFloatingNumber = numberSrt.includes('.');
  if (!isFloatingNumber) {
    return fixed ? parseFloat(numberSrt).toFixed(fixed) : number;
  }

  const intDigits = numberSrt.split('.')[0].length;
  const floatDigits = numberSrt.split('.')[1]?.length;

  let endIndex: number;

  if (swing) {
    endIndex =
      intDigits +
      1 +
      (maxFloatDigits > intDigits ? maxFloatDigits + 1 - intDigits : 0);
  } else {
    endIndex =
      intDigits +
      1 +
      (floatDigits > maxFloatDigits
        ? fixed || maxFloatDigits
        : fixed || floatDigits);
  }

  if (endIndex > numberSrt.length - 1) {
    for (let i = 0; i < endIndex - numberSrt.length; i++) {
      numberSrt += '0';
    }
  }

  return numberSrt.slice(0, endIndex);
};
export default class NumbersFormatter {
  static formatToAmount = (number?: number | string): number | string => {
    return toFixed({ number, swing: true });
  };

  static formatFromAmount = (number?: number | string): number | string => {
    return toFixed({ number });
  };

  static formatPriceImpact = (number?: number | string): number | string => {
    return toFixed({ number, fixed: priceImpactFloatDigits });
  };

  static formatBalance = (number?: number | string): number | string => {
    return toFixed({ number, fixed: balanceFloatDigits });
  };

  static formatOtherNumber = (number?: number | string): number | string => {
    return toFixed({ number });
  };
}
