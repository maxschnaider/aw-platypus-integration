import React from 'react';
import { Flex, HStack } from '@chakra-ui/react';
import { Tab } from './Tab';

const tabs = [
  {
    name: 'Swap',
    path: '/swap',
  },
  {
    name: 'Pool',
    path: '/pool',
  },
];

export const NavigationTabs = () => (
  <Flex
    className="NavigationTabs__Container"
    as="div"
    align="center"
    wrap="wrap"
    w="100%"
  >
    <HStack className="NavigationTabs" m="auto" spacing={-8}>
      {tabs.map((tabProps, index) => (
        <Tab name={tabProps.name} path={tabProps.path} key={index} />
      ))}
    </HStack>
  </Flex>
);
