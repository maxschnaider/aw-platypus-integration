import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Text, Button } from '@chakra-ui/react';

export const Tab = ({ name, path }: { name: string; path: string }) => {
  // const { pathname } = useLocation();

  // const isActive = path === pathname;
  const isActive = path === '/swap';

  return (
    // <Link to={path}>
    <Button
      className="NavigationTabs__Tab"
      maxW="100%"
      w="150px"
      p={4}
      textAlign="center"
      bg="blue.900"
      _hover={{ bg: '' }}
      _active={{ bg: '' }}
      color="white"
      variant={isActive ? 'solid' : 'ghost'}
      disabled={!isActive}
      border="1px solid rgb(0, 80, 255)"
      borderRadius="full"
      zIndex={isActive ? 1 : 0}
    >
      <Text fontSize="2xl">{name}</Text>
    </Button>
    // </Link>
  );
};
