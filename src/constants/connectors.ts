import { DEFAULT_CHAIN } from './';
import { MetaMaskConnector } from 'wagmi/connectors/metaMask';

export const CONNECTORS = () => {
  const connector = new MetaMaskConnector({
    chains: [DEFAULT_CHAIN],
  });
  return [connector];
};
