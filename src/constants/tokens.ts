import { Token } from '../types';
import { ETH_ADDRESS } from './addresses';

export const TOKENS_IMAGE_BASE_URL = 'https://app.platypus.finance/tokens';

export const tokens: Record<string, Token> = {
  AVAX: {
    id: ETH_ADDRESS,
    symbol: 'AVAX',
    name: 'Avalanche',
    decimals: 18,
    logoURI: `${TOKENS_IMAGE_BASE_URL}/avax`,
    logoFormat: 'svg',
    isEth: true,
    wrappedAddress: '0xB31f66AA3C1e785363F0875A1B74E27b85FD66c7',
  },
};
