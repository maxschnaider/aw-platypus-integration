/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import * as React from 'react';
import { SwapView } from '.';
import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import { WagmiConfig, createClient } from 'wagmi';
import { CONNECTORS } from '../constants';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Fonts, FocusInvisibility, TransparentScrollbar } from '../components';

const wagmiClient = createClient({
  autoConnect: true,
  connectors: CONNECTORS,
});

const queryClient = new QueryClient();

const theme = extendTheme({
  fonts: {
    heading: `CyborgSister,Helvetica,Arial,sans-serif `,
    body: `CyborgSister,Helvetica,Arial,sans-serif`,
  },
});

const App = () => {
  return (
    <WagmiConfig client={wagmiClient}>
      <QueryClientProvider client={queryClient}>
        <ChakraProvider theme={theme}>
          <FocusInvisibility />
          <Fonts />
          <TransparentScrollbar />
          <SwapView />
        </ChakraProvider>
      </QueryClientProvider>
    </WagmiConfig>
  );
};

export default App;
