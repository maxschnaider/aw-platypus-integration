import React, { useEffect } from 'react';
import {
  Flex,
  Box,
  Grid,
  GridItem,
  Button,
  Image,
  Text,
  NumberInput,
  NumberInputField,
} from '@chakra-ui/react';
import { FaCoins } from 'react-icons/fa';
import { Token, SwapSide } from '../../../../types';
import { NumbersFormatter } from '../../../../utils';
import { useTokenBalance } from '../../../../hooks';

export interface TokenInputPanelProps {
  swapSide: SwapSide;
  handleSelectTokenButtonClick: () => void;
  token?: Token;
  amount?: string;
  handleAmountChange?: (amount: string) => void;
}

export const TokenInputPanel = ({
  swapSide,
  handleSelectTokenButtonClick,
  token,
  amount,
  handleAmountChange,
}: TokenInputPanelProps) => {
  const { data: tokenBalance, refetch: checkTokenBalance } =
    useTokenBalance(token);

  useEffect(() => {
    if (token && (!amount || parseFloat(amount) === 0))
      checkTokenBalance().catch((e) => console.error(e));
  }, [amount]);

  const handleInputChange = (newAmount?: string | undefined) => {
    if (!handleAmountChange) return;
    if (!newAmount) {
      newAmount = '0';
    } else if (
      newAmount.length >= 2 &&
      newAmount.charAt(0) === '0' &&
      newAmount.charAt(1) !== '.'
    ) {
      newAmount = newAmount.slice(1);
    } else if (
      swapSide === SwapSide.From &&
      tokenBalance &&
      parseFloat(tokenBalance) < parseFloat(newAmount)
    ) {
      return setMaxAmount();
    }
    handleAmountChange(newAmount);
  };

  const setMaxAmount = () => {
    if (tokenBalance && handleAmountChange) {
      handleAmountChange(tokenBalance);
    }
  };

  const amountToDisplay =
    swapSide === SwapSide.To
      ? NumbersFormatter.formatToAmount(amount)
      : NumbersFormatter.formatFromAmount(amount);

  const tokenIcon = !token ? (
    <FaCoins />
  ) : (
    <Image
      srcSet={`${token.logoURI}.${token.logoFormat}`}
      borderRadius="full"
      h={25}
    />
  );

  const renderTokenButton = () => (
    <GridItem w="100%" h="10" colSpan={[6, 2]}>
      <Button
        w="100%"
        variant="outline"
        colorScheme=""
        leftIcon={tokenIcon}
        onClick={handleSelectTokenButtonClick}
      >
        <Text fontSize="22px">{token?.symbol || 'Select a token'}</Text>
      </Button>
    </GridItem>
  );

  const renderInput = () => (
    <GridItem
      w="100%"
      h="10"
      colSpan={swapSide === SwapSide.From ? [5, 3] : [6, 4]}
    >
      <NumberInput
        variant="unstyled"
        isDisabled={!token || swapSide === SwapSide.To}
        size="lg"
        lineHeight={10}
        min={0}
        value={amountToDisplay}
        onChange={(newAmount) => handleInputChange(newAmount)}
      >
        <NumberInputField p={0} textAlign="right" fontSize="3xl" />
      </NumberInput>
    </GridItem>
  );

  const renderMaxButton = () => {
    return (
      <GridItem w="100%" h="10" colSpan={1}>
        <Button
          w="100%"
          colorScheme="blue"
          variant="outline"
          textTransform="uppercase"
          disabled={!token}
          onClick={setMaxAmount}
        >
          <Text fontSize="2xl">Max</Text>
        </Button>
      </GridItem>
    );
  };

  return (
    <Box className="TokenInputPanel__Container" as="div" w="100%">
      <Flex
        className="TokenInputPanel__Subheader"
        align="center"
        justify="space-between"
        w="100%"
      >
        <Text fontSize="21px">{SwapSide[swapSide]}</Text>
        <Text fontSize="21px">
          Balance: {NumbersFormatter.formatBalance(tokenBalance)}
        </Text>
      </Flex>
      <Box
        className="TokenInputPanel"
        as="div"
        w="100%"
        paddingY={3}
        paddingX={5}
        bg="blackAlpha.600"
        borderRadius={10}
        marginTop={1}
      >
        <Grid templateColumns="repeat(6, 1fr)" gap={4}>
          {renderTokenButton()}
          {renderInput()}
          {swapSide === SwapSide.From && renderMaxButton()}
        </Grid>
      </Box>
    </Box>
  );
};
