import React, { useState, useEffect } from 'react';
import {
  Box,
  Input,
  Stack,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/react';
import { Token } from '../../../../types';
import { TokensListItem } from '.';
import BorderImage from '../../../../assets/images/border.png';

export interface TokenListModalProps {
  isOpen: boolean;
  onClose: () => void;
  tokensList?: Token[];
  handleTokenSelect: (token: Token) => void;
}

export const TokensListModal = ({
  isOpen,
  onClose,
  tokensList,
  handleTokenSelect,
}: TokenListModalProps) => {
  const [searchText, setSearchText] = useState<string>('');

  useEffect(() => {
    setSearchText('');
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay bg="blackAlpha.300" backdropFilter="blur(10px)" />
      <ModalContent
        color="white"
        className="TokensListModal"
        m={4}
        bg="none"
        maxH="90%"
        style={{
          borderImage: `url(${BorderImage})`,
          borderStyle: 'solid',
          borderWidth: '30px',
          borderImageSlice: '30 fill',
          borderImageRepeat: 'round',
        }}
      >
        <ModalHeader fontSize="3xl" pb={0}>
          Select a token
          <Box pt={2}>
            <Input
              className="TokensListModal__Search"
              variant="unstyled"
              placeholder="Search Token"
              size="lg"
              bg="blackAlpha.600"
              p={3}
              onChange={(e) => setSearchText(e.target.value)}
              autoFocus={false}
              fontSize="xl"
            />
          </Box>
        </ModalHeader>
        <ModalCloseButton />
        <ModalBody p={0} overflowY="auto" style={{ scrollbarWidth: 'none' }}>
          <Stack
            className="TokensListModal__List"
            paddingTop={3}
            paddingBottom={5}
          >
            {!!tokensList &&
              tokensList
                .filter((token) =>
                  token.symbol.toLowerCase().includes(searchText.toLowerCase())
                )
                .sort((tokenA, tokenB) => {
                  const symbolA = tokenA.symbol.toLowerCase();
                  const symbolB = tokenB.symbol.toLowerCase();
                  return symbolA.localeCompare(symbolB);
                })
                .map((token) => (
                  <TokensListItem
                    key={token.id}
                    handleTokenSelect={handleTokenSelect}
                    token={token}
                  />
                ))}
          </Stack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
