import React from 'react';
import { Flex, Image, Text, HStack } from '@chakra-ui/react';
import { Token } from '../../../../types';
import { NumbersFormatter } from '../../../../utils';
import { useTokenBalance } from '../../../../hooks';

interface TokensListItemProps {
  token: Token;
  handleTokenSelect: (token: Token) => void;
}

export const TokensListItem = ({
  token,
  handleTokenSelect,
}: TokensListItemProps) => {
  const { data: tokenBalance } = useTokenBalance(token);

  return (
    <Flex
      className="TokensListModal__Token"
      as="div"
      align="center"
      justify="space-between"
      w="100%"
      paddingX={6}
      paddingY={2}
      _hover={{
        bg: 'blackAlpha.600',
        cursor: 'pointer',
      }}
      key={token.id}
      onClick={() => handleTokenSelect(token)}
    >
      <HStack spacing={2}>
        <Image
          className="TokensListModal__Token-Image"
          srcSet={`${token.logoURI}.${token.logoFormat}`}
          onError={(e) => {
            if (token.logoFormat === 'png') {
              token.logoFormat = 'svg';
              e.currentTarget.srcset = `${token.logoURI}.${token.logoFormat}`;
            }
          }}
          borderRadius="full"
          h={30}
        />
        <Text className="TokensListModal__Token-Symbol" fontSize="2xl">
          {token.symbol}
        </Text>
      </HStack>
      <Text className="TokensListModal__Token-Balance" fontSize="2xl">
        {NumbersFormatter.formatBalance(tokenBalance)}
      </Text>
    </Flex>
  );
};
