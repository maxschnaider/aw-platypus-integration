/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import React from 'react';
import { Button, Text } from '@chakra-ui/react';
import { useConnect, useNetwork } from 'wagmi';
import { DEFAULT_CHAIN } from '../../../../constants';
import { SwapVariant, SwapViewState } from '../../../../types';

export interface SwapButtonProps {
  swapState: SwapViewState;
  swapVariant: SwapVariant;
  handleSwap: () => void;
  handleApprove: () => void;
}

export const SwapButton = ({
  swapState,
  swapVariant,
  handleSwap,
  handleApprove,
}: SwapButtonProps) => {
  const { connect, connectors } = useConnect();
  const { switchNetwork } = useNetwork();

  const swapButtonText = () => {
    if (swapState === SwapViewState.NoSigner) {
      return 'Connect Wallet';
    }
    if (swapState === SwapViewState.WrongNetwork) {
      return 'Switch Network';
    }
    if (swapState === SwapViewState.LoadingQuote) {
      return 'Waiting for quotes...';
    }
    if (swapState === SwapViewState.ErrorFetchingQuote) {
      return 'Swapping not allowed';
    }
    if (swapState === SwapViewState.RequiresApprove) {
      return 'Approve';
    }
    if (swapState === SwapViewState.LoadingApprove) {
      return 'Approving...';
    }
    if (swapState === SwapViewState.ReadyToSwap) {
      return swapVariant === SwapVariant.Wrap
        ? 'Wrap'
        : swapVariant === SwapVariant.Unwrap
        ? 'Unwrap'
        : 'Swap';
    }
    if (swapState === SwapViewState.LoadingSwap) {
      return 'Waiting for Confirmation...';
    }
    if (swapState === SwapViewState.NoAmount) {
      return 'Enter Amount';
    }
    return 'Select Tokens';
  };

  const handleSwapButtonClick = () => {
    if (swapState === SwapViewState.NoSigner) {
      return connect(connectors[0]);
    }
    if (swapState === SwapViewState.WrongNetwork) {
      return switchNetwork?.(DEFAULT_CHAIN.id);
    }
    if (swapState === SwapViewState.ReadyToSwap) {
      return handleSwap();
    }
    if (swapState === SwapViewState.RequiresApprove) {
      return handleApprove();
    }
  };

  const isLoading =
    swapState === SwapViewState.LoadingApprove ||
    swapState === SwapViewState.LoadingQuote ||
    swapState === SwapViewState.LoadingSwap;

  const renderSwapButton = () => (
    <Button
      colorScheme=""
      background="linear-gradient(90deg, rgb(0, 241, 82) 0%, rgb(0, 118, 233) 100%);"
      onClick={handleSwapButtonClick}
      h="50px"
      isLoading={isLoading}
      loadingText={swapButtonText()}
      style={{ fontSize: '28px' }}
      backgroundSize="120% 100%"
      transition={`background 1s`}
      _hover={{
        backgroundPosition: '100%',
      }}
    >
      <Text fontSize="3xl">{swapButtonText()}</Text>
    </Button>
  );

  return renderSwapButton();
};
