import React from 'react';
import {
  Text,
  Stack,
  HStack,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  Image,
  Link,
} from '@chakra-ui/react';
import { FaCheckCircle } from 'react-icons/fa';
import { Token } from '../../../../types';
import { ContractTransaction } from 'ethers';
import BorderImage from '../../../../assets/images/border.png';

export interface TransactionModalProps {
  isOpen: boolean;
  onClose: () => void;
  transaction?: ContractTransaction;
  token?: Token;
}

export const SwapSubmittedModal = ({
  isOpen,
  onClose,
  transaction,
  token,
}: TransactionModalProps) => {
  const renderTxButton = () => (
    <Link
      href={`https://snowtrace.io/tx/${transaction?.hash || ''}`}
      isExternal
    >
      <Button
        w="100%"
        variant="outline"
        colorScheme=""
        size="lg"
        bg="blackAlpha.400"
      >
        <Text fontSize="2xl">View on SnowTrace</Text>
      </Button>
    </Link>
  );

  const tokenIcon = token && (
    <Image
      srcSet={`${token.logoURI}.${token.logoFormat}`}
      borderRadius="full"
      h={25}
      marginX={2}
    />
  );

  const renderAddTokenButton = () => (
    <Button variant="outline" colorScheme="" size="lg" bg="blackAlpha.400">
      <Text fontSize="2xl">Add</Text> {tokenIcon}
      <Text fontSize="2xl">{token?.symbol} to wallet</Text>
    </Button>
  );

  const renderDoneButton = () => (
    <Button
      variant="solid"
      colorScheme=""
      size="lg"
      onClick={onClose}
      background="linear-gradient(90deg, rgb(0, 241, 82) 0%, rgb(0, 118, 233) 100%);"
    >
      <Text fontSize="2xl">Done</Text>
    </Button>
  );

  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay bg="blackAlpha.300" backdropFilter="blur(10px)" />
      <ModalContent
        color="white"
        className="SwapConfirmModal"
        bg="none"
        style={{
          borderImage: `url(${BorderImage})`,
          borderStyle: 'solid',
          borderWidth: '30px',
          borderImageSlice: '30 fill',
          borderImageRepeat: 'round',
        }}
      >
        <ModalHeader>
          <HStack>
            <Text fontSize="3xl">Transaction submitted</Text>
            <FaCheckCircle />
          </HStack>
        </ModalHeader>
        <ModalCloseButton />
        <ModalBody p={5}>
          <Stack>
            {renderTxButton()}
            {!token?.isEth && renderAddTokenButton()}
            {renderDoneButton()}
          </Stack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
