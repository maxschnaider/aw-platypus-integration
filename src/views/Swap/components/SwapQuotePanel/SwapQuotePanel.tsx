import React, { ReactNode, useEffect, useState } from 'react';
import { Flex, Box, Image, Text, Stack, HStack } from '@chakra-ui/react';
import { SwapQuote } from '../../../../hooks';
import { NumbersFormatter } from '../../../../utils';
import { SwapVariant, Token } from '../../../../types';

export interface SwapQuotePanelProps {
  swapQuote?: SwapQuote;
  isLoading: boolean;
  swapVariant: SwapVariant;
}

export const SwapQuotePanel = ({
  swapQuote,
  isLoading,
  swapVariant,
}: SwapQuotePanelProps) => {
  const { formatOtherNumber, formatPriceImpact, formatToAmount } =
    NumbersFormatter;

  const [swapQuoteTokens, setSwapQuoteTokens] = useState<Token[]>([]);
  const [fromTokenSymbol, setFromTokenSymbol] = useState('');
  const [toTokenSymbol, setToTokenSymbol] = useState('');
  const [rate, setRate] = useState<string | number>('');
  const [priceImpact, setPriceImpact] = useState<string | number>('');
  const [fee, setFee] = useState<string | number>('');
  const [minimumToAmount, setMinimumToAmount] = useState<string | number>('');

  useEffect(() => {
    if (!swapQuote) return;

    const {
      tokenPath: swapQuoteTokens,
      rate: swapQuoteRate,
      priceImpact: swapQuotePriceImpact,
      fee: swapQuoteFee,
      minimumToAmount: swapQuoteMinimumToAmount,
    } = swapQuote;

    const fromTokenSymbol = swapQuoteTokens[0].symbol;
    const toTokenSymbol = swapQuoteTokens[swapQuoteTokens.length - 1]!.symbol;
    const rate = formatOtherNumber(swapQuoteRate);
    const priceImpact = formatPriceImpact(swapQuotePriceImpact);
    const fee = formatOtherNumber(swapQuoteFee);
    const minimumToAmount = formatToAmount(swapQuoteMinimumToAmount);

    setSwapQuoteTokens(swapQuoteTokens);
    setFromTokenSymbol(fromTokenSymbol);
    setToTokenSymbol(toTokenSymbol);
    setRate(rate);
    setPriceImpact(priceImpact);
    setFee(fee);
    setMinimumToAmount(minimumToAmount);
  }, [swapQuote]);

  const renderFlexWrapper = (renderContent: () => ReactNode): ReactNode => {
    return (
      <Flex
        className="SwapDetailsPanel__Row"
        align="center"
        justify="space-between"
        w="100%"
      >
        {renderContent()}
      </Flex>
    );
  };

  const renderRate = () => {
    return renderFlexWrapper(() => (
      <>
        <Text fontSize="xl">Rate (after fee)</Text>
        <Text fontSize="xl">
          {isLoading
            ? '...'
            : `1 ${fromTokenSymbol} = ${rate} ${toTokenSymbol}`}
        </Text>
      </>
    ));
  };

  const renderPriceImpact = () => {
    return renderFlexWrapper(() => (
      <>
        <Text fontSize="xl">Price Impact</Text>
        <Text fontSize="xl">{isLoading ? '...' : `${priceImpact}%`}</Text>
      </>
    ));
  };

  const renderFee = () => {
    return renderFlexWrapper(() => (
      <>
        <Text fontSize="xl">Fee</Text>
        <Text fontSize="xl">
          {isLoading ? '...' : `${fee} ${toTokenSymbol}`}
        </Text>
      </>
    ));
  };

  const renderMinimumToAmount = () => {
    return renderFlexWrapper(() => (
      <>
        <Text fontSize="xl">Minimum Received</Text>
        <Text fontSize="xl">
          {isLoading ? '...' : `${minimumToAmount} ${toTokenSymbol}`}
        </Text>
      </>
    ));
  };

  const renderRoute = () => (
    <Stack className="SwapDetailsPanel__Row" w="100%">
      <Text fontSize="xl">Route</Text>
      <HStack
        className="SwapDetailsPanel__Row-Route"
        justify="center"
        w="100%"
        border="1px"
        borderColor="teal.700"
        p={2}
        borderRadius={7}
      >
        {isLoading || !swapQuoteTokens.length ? (
          <Text h={30} fontSize="xl">
            ...
          </Text>
        ) : (
          swapQuoteTokens.map((token, index) => (
            <HStack spacing={2} key={token.id}>
              <Image
                srcSet={`${token.logoURI}.${token.logoFormat}`}
                borderRadius="full"
                h={25}
              />
              <Text h={30} fontSize="xl">
                {token.symbol}
              </Text>
              {index !== swapQuoteTokens.length - 1 && <Text> → </Text>}
            </HStack>
          ))
        )}
      </HStack>
    </Stack>
  );

  return (
    <Box className="SwapDetailsPanel">
      <Stack>
        {renderRate()}
        {swapVariant !== SwapVariant.Wrap &&
          swapVariant !== SwapVariant.Unwrap &&
          renderPriceImpact()}
        {swapVariant !== SwapVariant.Wrap &&
          swapVariant !== SwapVariant.Unwrap &&
          renderFee()}
        {renderMinimumToAmount()}
        {renderRoute()}
      </Stack>
    </Box>
  );
};
