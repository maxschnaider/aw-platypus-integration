import React, { useState, useEffect, useCallback } from 'react';
import { Token, SwapSide, SwapViewState, SwapVariant } from '../../types';
import {
  useTokensList,
  useCheckAllowance,
  useBuildSwapPaths,
  useQuote,
  useApprove,
  useSwap,
} from '../../hooks';
import { useNetwork, useSigner } from 'wagmi';
import {
  useDirectTokensForToken,
  useMultihopTokensForToken,
} from '../../hooks/helpers';
import { Flex, Box, Stack, IconButton, useDisclosure } from '@chakra-ui/react';
import {
  TokenInputPanel,
  TokensListModal,
  SwapButton,
  SwapQuotePanel,
  SwapSubmittedModal,
} from './components';
import { NavigationTabs } from '../../components';
import { MdSwapVert } from 'react-icons/md';
import BorderImage from '../../assets/images/border.png';

export const SwapView = () => {
  const {
    isOpen: isTokensListModalOpen,
    onOpen: onTokensListModalOpen,
    onClose: onTokensListModalClose,
  } = useDisclosure();
  const {
    isOpen: isSwapSubmittedModalOpen,
    onOpen: onSwapSubmittedModalOpen,
    onClose: onSwapSubmittedModalClose,
  } = useDisclosure();

  const [swapSide, setSwapSide] = useState(SwapSide.From);
  const [swapVariant, setSwapVariant] = useState(
    SwapVariant.TokenToTokenDirect
  );

  const { data: signer } = useSigner();
  const { activeChain } = useNetwork();

  /**
   * TOKENS
   */
  const { data: tokensList } = useTokensList();
  const [pairsTokensList, setPairsTokensList] = useState<Token[]>([]);

  const [fromToken, setFromToken] = useState<Token | undefined>();
  const [toToken, setToToken] = useState<Token | undefined>();

  const { data: directPairsTokensList } = useDirectTokensForToken(
    fromToken || toToken
  );
  const { data: multihopPairsTokensList } = useMultihopTokensForToken(
    fromToken || toToken
  );

  useEffect(() => {
    setPairsTokensList([
      ...(directPairsTokensList || []),
      ...(multihopPairsTokensList || []),
    ]);
  }, [directPairsTokensList, multihopPairsTokensList]);

  const resetSwapTokens = () => {
    setFromToken(undefined);
    setToToken(undefined);
  };

  const handleSwitchTokens = useCallback(() => {
    const _fromToken = fromToken;
    setFromToken(toToken);
    setToToken(_fromToken);
  }, [fromToken, toToken]);

  const handleTokenSelect = useCallback(
    (token: Token, swapSide: SwapSide) => {
      if (swapSide === SwapSide.From) {
        if (fromToken) resetSwapTokens(); // reset tokens on token replacement to prevent impossible pair
        setFromToken(token);
      } else {
        if (toToken) resetSwapTokens();
        setToToken(token);
      }
    },
    [fromToken, toToken]
  );

  const getTokensListToDisplay = () => {
    const showPairsTokensList =
      (fromToken && !toToken && swapSide === SwapSide.To) ||
      (!fromToken && toToken && swapSide === SwapSide.From);
    return showPairsTokensList ? pairsTokensList : tokensList;
  };
  /**
   * TOKENS END
   */

  /**
   * AMOUNT
   */
  const [fromAmount, setFromAmount] = useState<string>('0');

  const resetAmounts = () => {
    setFromAmount('0');
  };

  useEffect(() => {
    resetAmounts();
  }, [fromToken, toToken]);
  /**
   * AMOUNT END
   */

  /**
   * QUOTE
   */
  const { data: swapPaths } = useBuildSwapPaths({
    fromToken: fromToken,
    toToken: toToken,
  });

  useEffect(() => {
    if (!swapPaths) return;
    let _swapVariant: SwapVariant;
    const ethToken = swapPaths.tokenPath.find((token) => token.isEth);
    if (ethToken) {
      if (
        swapPaths.tokenPath.some(
          (token) =>
            token.id.toLowerCase() === ethToken.wrappedAddress!.toLowerCase()
        )
      ) {
        _swapVariant = fromToken?.isEth ? SwapVariant.Wrap : SwapVariant.Unwrap;
      } else {
        _swapVariant = fromToken?.isEth
          ? SwapVariant.EthToToken
          : SwapVariant.TokenToEth;
      }
    } else if (swapPaths.tokenPath.length === 3) {
      _swapVariant = SwapVariant.TokenToTokenMultihop;
    } else {
      _swapVariant = SwapVariant.TokenToTokenDirect;
    }
    setSwapVariant(_swapVariant);
  }, [swapPaths]);

  const {
    data: swapQuoteData,
    isLoading: isLoadingQuote,
    quote,
  } = useQuote({
    fromToken: fromToken,
    toToken: toToken,
    fromAmount: fromAmount.toString(),
    tokenPath: swapPaths?.tokenPath,
    poolPath: swapPaths?.poolPath,
    swapVariant: swapVariant,
  });

  useEffect(() => {
    if (currentState() < SwapViewState.LoadingQuote) return;
    quote().catch((e) => console.error(e));
  }, [fromAmount]);
  /**
   * QUOTE END
   */

  /**
   * APPROVE
   */
  const {
    data: allowedAmount,
    isLoading: isLoadingAllowance,
    checkAllowance,
  } = useCheckAllowance({
    tokenAddress: fromToken?.id,
    spenderAddress: swapPaths?.poolPath[0].id,
  });
  const {
    data: approveTransaction,
    isLoading: isLoadingApprove,
    approve,
  } = useApprove({
    token: fromToken,
    spenderAddress: swapPaths?.poolPath[0].id,
    amount: fromAmount.toString(),
  });

  useEffect(() => {
    if (
      currentState() < SwapViewState.LoadingApprove ||
      swapVariant === SwapVariant.Wrap ||
      swapVariant === SwapVariant.Unwrap ||
      swapVariant === SwapVariant.EthToToken
    )
      return;
    checkAllowance().catch((e) => console.error(e));
  }, [swapQuoteData, approveTransaction]);
  /**
   * APPROVE END
   */

  /**
   * SWAP
   */
  const {
    data: swapData,
    isLoading: isLoadingSwap,
    swap,
  } = useSwap(swapQuoteData);

  useEffect(() => {
    if (currentState() != SwapViewState.SwapSubmitted) return;
    onSwapSubmittedModalOpen();
  }, [swapData, isLoadingSwap]);
  /**
   * SWAP END
   */

  const currentState = (): SwapViewState => {
    if (!signer) {
      return SwapViewState.NoSigner;
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (activeChain?.unsupported) {
      return SwapViewState.WrongNetwork;
    }
    if (!fromToken || !toToken || !swapPaths) {
      return SwapViewState.NoTokens;
    }
    if (parseFloat(fromAmount) === 0) {
      return SwapViewState.NoAmount;
    }
    if (isLoadingQuote) {
      return SwapViewState.LoadingQuote;
    }
    if (!swapQuoteData) {
      return SwapViewState.ErrorFetchingQuote;
    }
    if (isLoadingAllowance || isLoadingApprove) {
      return SwapViewState.LoadingApprove;
    }
    if (
      allowedAmount! < parseFloat(fromAmount) &&
      !(
        swapVariant === SwapVariant.Wrap ||
        swapVariant === SwapVariant.Unwrap ||
        swapVariant === SwapVariant.EthToToken
      )
    ) {
      return SwapViewState.RequiresApprove;
    }
    if (isLoadingSwap) {
      return SwapViewState.LoadingSwap;
    }
    if (swapData) {
      return SwapViewState.SwapSubmitted;
    }
    return SwapViewState.ReadyToSwap;
  };

  return (
    <Flex
      className="Swap__Container"
      as="div"
      align="center"
      wrap="wrap"
      w="100%"
      p={4}
    >
      <Box
        className="Swap"
        as="div"
        m="auto"
        w={600}
        maxW="100%"
        p={2}
        color="white"
        style={{
          borderImage: `url(${BorderImage})`,
          borderStyle: 'solid',
          borderWidth: '30px',
          borderImageSlice: '30 fill',
          borderImageRepeat: 'round',
        }}
      >
        <NavigationTabs />

        <Stack spacing={4} mt={2}>
          <Stack spacing={[0, -5]} align="center">
            <TokenInputPanel
              swapSide={SwapSide.From}
              handleSelectTokenButtonClick={() => {
                setSwapSide(SwapSide.From);
                onTokensListModalOpen();
              }}
              token={fromToken}
              amount={fromAmount}
              handleAmountChange={(amount) => setFromAmount(amount)}
            />

            <IconButton
              aria-label="Switch swap tokens"
              h="55px"
              w="55px"
              borderRadius="full"
              bg="blackAlpha.600"
              size="lg"
              colorScheme=""
              icon={<MdSwapVert />}
              top="18px"
              onClick={handleSwitchTokens}
              _hover={{ bg: 'blue.900' }}
            />

            <TokenInputPanel
              swapSide={SwapSide.To}
              handleSelectTokenButtonClick={() => {
                setSwapSide(SwapSide.To);
                onTokensListModalOpen();
              }}
              token={toToken}
              amount={swapQuoteData?.toAmount}
              handleAmountChange={undefined}
            />
          </Stack>

          {(currentState() > SwapViewState.ErrorFetchingQuote ||
            currentState() === SwapViewState.LoadingQuote) && (
            <SwapQuotePanel
              swapQuote={swapQuoteData}
              isLoading={isLoadingQuote}
              swapVariant={swapVariant}
            />
          )}

          <SwapButton
            swapState={currentState()}
            swapVariant={swapVariant}
            handleSwap={swap}
            handleApprove={approve}
          />
        </Stack>
      </Box>

      <TokensListModal
        isOpen={isTokensListModalOpen}
        onClose={onTokensListModalClose}
        tokensList={getTokensListToDisplay()}
        handleTokenSelect={(token) => {
          handleTokenSelect(token, swapSide);
          onTokensListModalClose();
        }}
      />

      <SwapSubmittedModal
        isOpen={isSwapSubmittedModalOpen}
        onClose={() => {
          resetAmounts();
          onSwapSubmittedModalClose();
        }}
        transaction={swapData?.transaction}
        token={toToken}
      />
    </Flex>
  );
};
