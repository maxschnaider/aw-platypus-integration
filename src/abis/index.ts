import IPlatypusRouter from './PlatypusRouter.json';
import IPlatypusPool from './PlatypusPool.json';
import IERC20 from './ERC20.json';
import IWrappedERC20 from './WrappedERC20.json';

export { IPlatypusRouter, IPlatypusPool, IERC20, IWrappedERC20 };
