/* eslint-disable @typescript-eslint/no-unnecessary-type-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { PlatypusPool__factory } from '../contracts';
import { useSigner } from 'wagmi';
import { useQuery } from 'react-query';

export const usePlatypusPoolContract = (address?: string | undefined) => {
  const { data: signer } = useSigner();
  return useQuery(
    ['get-platypus-pool-contract', address],
    () => {
      return PlatypusPool__factory.connect(address!, signer!);
    },
    {
      enabled: !!signer && !!address,
    }
  );
};
