import { BigNumber, utils } from 'ethers';
import { useQuery } from 'react-query';
import { Pool, SwapVariant, Token } from '../types';
import { usePlatypusRouterContract } from '.';

export interface SwapQuote {
  fromToken: Token;
  toToken: Token;
  fromAmount: string;
  toAmount: string;
  rate: string | number;
  priceImpact: string | number;
  fee: string | number;
  minimumToAmount: string;
  tokenPath: Token[];
  poolPath: Pool[];
  swapVariant: SwapVariant;
}

export interface UseQuoteParams {
  fromToken?: Token;
  toToken?: Token;
  fromAmount?: string;
  tokenPath?: Token[];
  poolPath?: Pool[];
  swapVariant: SwapVariant;
}

export interface QuoteParams {
  tokenPath: string[];
  poolPath: string[];
  fromAmount: BigNumber;
}

export const useQuote = ({
  fromToken,
  toToken,
  fromAmount,
  tokenPath,
  poolPath,
  swapVariant,
}: UseQuoteParams) => {
  const { data: platypusRouterContract } = usePlatypusRouterContract();
  let slippagePercent = 1;

  const { data, error, isLoading, isRefetching, refetch } = useQuery(
    ['swap-quote', fromToken?.id, toToken?.id, fromAmount],
    async () => {
      const params: QuoteParams = {
        tokenPath: tokenPath!.map((token) =>
          token.isEth ? token.wrappedAddress! : token.id
        ),
        poolPath: poolPath!.map((pool) => pool.id),
        fromAmount: utils.parseUnits(fromAmount!, fromToken!.decimals),
      };

      let toAmountBN: BigNumber, feeBN: BigNumber;

      if (
        swapVariant === SwapVariant.Wrap ||
        swapVariant === SwapVariant.Unwrap
      ) {
        toAmountBN = params.fromAmount;
        feeBN = utils.parseEther('0');
        slippagePercent = 0;
      } else {
        const response = await platypusRouterContract!.quotePotentialSwaps(
          params.tokenPath,
          params.poolPath,
          params.fromAmount
        );
        toAmountBN = response.potentialOutcome;
        feeBN = response.haircut;
      }

      // console.log(
      //   parseFloat(fromAmount!) * (toToken!.price! - fromToken!.price!)
      // );

      const toAmount = utils.formatUnits(toAmountBN, toToken!.decimals);
      const rate = parseFloat(toAmount) / parseFloat(fromAmount!);
      const marketRate = toToken!.price! / fromToken!.price!;
      const priceImpact = (1 - rate / marketRate) * 100;
      const fee = utils.formatUnits(feeBN, toToken!.decimals);
      const minimumToAmount = (
        parseFloat(toAmount) *
        (1 - slippagePercent / 100)
      ).toFixed(toToken!.decimals);

      const swapQuote: SwapQuote = {
        fromToken: fromToken!,
        toToken: toToken!,
        fromAmount: fromAmount!,
        toAmount: toAmount,
        rate: rate,
        priceImpact: priceImpact > 0 ? priceImpact : 0.0,
        fee: fee,
        minimumToAmount: minimumToAmount,
        tokenPath: tokenPath!,
        poolPath: poolPath!,
        swapVariant: swapVariant,
      };
      return swapQuote;
    },
    {
      enabled: false,
      cacheTime: 0,
    }
  );

  return {
    data,
    error,
    isLoading: isLoading || isRefetching,
    quote: refetch,
  } as const;
};
