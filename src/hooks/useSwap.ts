/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { BigNumber, ContractReceipt, ContractTransaction, utils } from 'ethers';
import { useQuery } from 'react-query';
import {
  SwapQuote,
  usePlatypusRouterContract,
  usePlatypusPoolContract,
  useWrappedErc20Contract,
} from '.';
import { useAccount } from 'wagmi';
import { SwapVariant } from '../types';

export interface SwapResponse {
  transaction: ContractTransaction;
  receipt: ContractReceipt;
}

export interface SwapTokensForTokensParams {
  fromToken: string;
  toToken: string;
  tokenPath: string[];
  poolPath: string[];
  fromAmount: BigNumber;
  minimumToAmount: BigNumber;
  toAddress: string;
  deadline: number;
}

export const useSwap = (swapQuote?: SwapQuote | undefined) => {
  const { data: account } = useAccount();
  const { data: platypusRouterContract } = usePlatypusRouterContract();
  const { data: platypusPoolContract } = usePlatypusPoolContract(
    swapQuote?.poolPath[0].id
  );
  const { data: wrappedErc20Contract } = useWrappedErc20Contract(
    swapQuote?.tokenPath?.find((token) => token.isEth)?.wrappedAddress
  );

  const deadlineSeconds = 60;

  const { data, error, isLoading, isRefetching, refetch } = useQuery(
    [
      'swap-tokens',
      swapQuote?.fromToken?.id,
      swapQuote?.toToken?.id,
      swapQuote?.fromAmount,
    ],
    async (): Promise<SwapResponse> => {
      const {
        fromToken,
        toToken,
        fromAmount,
        minimumToAmount,
        tokenPath,
        poolPath,
        swapVariant,
      } = swapQuote!;

      const params: SwapTokensForTokensParams = {
        fromToken: fromToken.id,
        toToken: toToken.id,
        tokenPath: tokenPath.map((token) => token.id),
        poolPath: poolPath.map((pool) => pool.id),
        fromAmount: utils.parseUnits(fromAmount, fromToken.decimals),
        minimumToAmount: utils.parseUnits(minimumToAmount, toToken.decimals),
        toAddress: account?.address as string,
        deadline: Math.floor(Date.now() / 1000) + deadlineSeconds,
      };

      let tx: ContractTransaction;

      if (swapVariant === SwapVariant.Wrap) {
        tx = await wrappedErc20Contract!.deposit({
          from: params.toAddress,
          value: params.fromAmount,
        });
      } else if (swapVariant === SwapVariant.Unwrap) {
        tx = await wrappedErc20Contract!.withdraw(params.fromAmount);
      } else if (swapVariant === SwapVariant.EthToToken) {
        tx = await platypusPoolContract!.swapFromETH(
          params.toToken,
          params.minimumToAmount,
          params.toAddress,
          params.deadline,
          {
            value: params.fromAmount,
          }
        );
      } else if (swapVariant === SwapVariant.TokenToEth) {
        tx = await platypusPoolContract!.swapToETH(
          params.fromToken,
          params.fromAmount,
          params.minimumToAmount,
          params.toAddress,
          params.deadline
        );
      } else if (swapVariant === SwapVariant.TokenToTokenDirect) {
        tx = await platypusPoolContract!.swap(
          params.fromToken,
          params.toToken,
          params.fromAmount,
          params.minimumToAmount,
          params.toAddress,
          params.deadline
        );
      } else if (swapVariant === SwapVariant.TokenToTokenMultihop) {
        tx = await platypusRouterContract!.swapTokensForTokens(
          params.tokenPath,
          params.poolPath,
          params.fromAmount,
          params.minimumToAmount,
          params.toAddress,
          params.deadline
        );
      }
      const receipt = await tx!.wait();
      return {
        transaction: tx!,
        receipt: receipt,
      };
    },
    {
      retry: false,
      enabled: false,
      cacheTime: 0,
    }
  );

  return {
    data,
    error,
    isLoading: isLoading || isRefetching,
    swap: refetch,
  } as const;
};
