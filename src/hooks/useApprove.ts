import { utils } from 'ethers';
import { useQuery } from 'react-query';
import { Token } from '../types';
import { useErc20Contract } from '.';

export interface UseApproveParams {
  token?: Token | undefined;
  spenderAddress?: string | undefined;
  amount?: string | undefined;
}

export const useApprove = ({
  token,
  spenderAddress,
  amount,
}: UseApproveParams) => {
  const { data: tokenContract } = useErc20Contract(token?.id);

  const { data, error, isLoading, isRefetching, refetch } = useQuery(
    ['approve-spending-token', token?.id, spenderAddress, amount],
    async () => {
      const tx = await tokenContract!.approve(
        spenderAddress!,
        utils.parseUnits(amount!, token!.decimals)
      );
      await tx.wait();
      return tx;
    },
    {
      retry: false,
      enabled: false,
      cacheTime: 0,
    }
  );

  return {
    data,
    error,
    isLoading: isLoading || isRefetching,
    approve: refetch,
  } as const;
};
