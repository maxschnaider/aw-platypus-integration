/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unnecessary-type-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { Token } from '../types';
import { useSigner, useAccount } from 'wagmi';
import { BigNumber, utils } from 'ethers';
import { useQuery } from 'react-query';
import { useWrappedErc20Contract } from './useWrappedErc20Contract';

export const useTokenBalance = (token?: Token | undefined) => {
  const { data: signer } = useSigner();
  const { data: account } = useAccount();
  const { data: tokenContract } = useWrappedErc20Contract(token?.id);

  return useQuery(
    ['fetch-token-balance', token?.id],
    async () => {
      let balanceBN: BigNumber | undefined;
      if (token!.isEth) {
        balanceBN = await signer!.getBalance();
      } else {
        balanceBN = await tokenContract!.balanceOf(account!.address as string);
      }
      const balance = utils.formatUnits(balanceBN, token!.decimals);
      return balance;
    },
    {
      enabled: !!signer && !!account && !!token && !!tokenContract,
      cacheTime: 0,
    }
  );
};
