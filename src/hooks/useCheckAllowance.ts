/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { useQuery } from 'react-query';
import { useAccount } from 'wagmi';
import { useErc20Contract } from '.';
import { utils } from 'ethers';

export interface UseCheckAllowanceParams {
  tokenAddress?: string;
  spenderAddress?: string;
}

export const useCheckAllowance = ({
  tokenAddress,
  spenderAddress,
}: UseCheckAllowanceParams) => {
  const { data: account } = useAccount();
  const { data: tokenContract } = useErc20Contract(tokenAddress);

  const { data, error, isLoading, isRefetching, refetch } = useQuery(
    ['check-allowance', tokenAddress, spenderAddress],
    async () => {
      const allowanceAmountBN = await tokenContract!.allowance(
        account?.address as string,
        spenderAddress!
      );
      const allowanceAmount = utils.formatUnits(
        allowanceAmountBN,
        await tokenContract!.decimals()
      );
      return parseFloat(allowanceAmount);
    },
    {
      enabled: false,
      cacheTime: 0,
    }
  );

  return {
    data,
    error,
    isLoading: isLoading || isRefetching,
    checkAllowance: refetch,
  } as const;
};
