import axios from 'axios';
import { Token } from '../types';
import { useQuery } from 'react-query';
import {
  PLATYPUS_GRAPH_URL,
  TOKENS_IMAGE_BASE_URL,
  tokens,
} from '../constants';

type TokensListGraphResponse = {
  data: {
    tokens: Token[];
  };
};

const tokensListGraphQuery = `query {
        tokens {
            id
            symbol
            decimals
            name
            price
        }
    }`;

export const useTokensList = () => {
  return useQuery(
    ['fetch-tokens-list'],
    async () => {
      const response = await axios.post<TokensListGraphResponse>(
        PLATYPUS_GRAPH_URL,
        {
          query: tokensListGraphQuery,
        }
      );
      const tokensList = response.data.data.tokens.map((token) => {
        token.price =
          typeof token.price === 'string'
            ? parseFloat(token.price)
            : token.price;
        token.logoURI = `${TOKENS_IMAGE_BASE_URL}/${token.symbol
          .split('.')[0]
          .toLowerCase()}`;
        token.logoFormat = 'png';
        return token;
      });

      tokensList.push(tokens.AVAX);

      return tokensList;
    },
    {
      refetchOnReconnect: false,
      refetchOnWindowFocus: false,
      retryDelay: 3000,
      retry: 1,
    }
  );
};
