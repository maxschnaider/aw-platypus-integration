import axios from 'axios';
import { PLATYPUS_GRAPH_URL, tokens } from '../constants';
import { Pool } from '../types';
import { useQuery } from 'react-query';

type PoolsListGraphResponse = {
  data: {
    pools: Pool[];
  };
};

const poolsListGraphQuery = `query {
        pools {
            id
            tokensList {
                id
                symbol
            }
        }
    }`;

export const usePoolsList = () => {
  return useQuery(
    ['fetch-pools-list'],
    async () => {
      const response = await axios.post<PoolsListGraphResponse>(
        PLATYPUS_GRAPH_URL,
        {
          query: poolsListGraphQuery,
        }
      );
      const { data } = response;
      const poolsList = data.data.pools.map((pool) => {
        // manually remove MIM from main pool (idk wtf it's doing there)
        if (pool.tokensList.length > 2) {
          pool.tokensList = pool.tokensList.filter(
            (token) => token.symbol != 'MIM'
          );
        }
        // also manually add AVAX to it's pool, since it's allowed to swap only in AVAXes pool
        if (pool.tokensList[0]?.symbol.toLocaleLowerCase().includes('avax')) {
          pool.tokensList.push({
            id: tokens.AVAX.id,
            symbol: tokens.AVAX.symbol,
          });
        }
        return pool;
      });
      return poolsList;
    },
    {
      refetchOnReconnect: false,
      refetchOnWindowFocus: false,
      retryDelay: 3000,
      retry: 1,
    }
  );
};
