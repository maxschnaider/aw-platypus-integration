import axios from 'axios';
import { Token } from '../types';
import { useQuery } from 'react-query';

type CoingeckoTokensListResponse = {
  id: string;
  symbol: string;
  name: string;
}[];

type CoingeckoTokenResponse = {
  id: string;
  symbol: string;
  platforms: {
    avalanche?: string;
  };
  market_data: {
    current_price: {
      usd: number | string;
    };
  };
  links: {
    blockchain_site: string[];
  };
  // image: {
  //   large: string;
  // };
};

type TokenPrice = {
  tokenId: string;
  price: number;
};

export const useTokensListPrices = (tokensList: Token[]) => {
  const apiBaseUrl = 'https://api.coingecko.com/api/v3';
  const tokenListEndpointUrl = apiBaseUrl + '/coins/list';
  const tokenEndpointUrl = apiBaseUrl + '/coins';

  return useQuery(
    ['fetch-tokens-list-prices'],
    async () => {
      const tokensListPrices: TokenPrice[] = [];
      const tokensListResponse = await axios.get<CoingeckoTokensListResponse>(
        tokenListEndpointUrl
      );
      const coingeckoTokensList = tokensListResponse.data.filter(
        (coingeckoToken) =>
          tokensList.some((token) =>
            token.symbol
              .toLowerCase()
              .includes(coingeckoToken.symbol.toLowerCase())
          )
      );

      for (const coingeckoToken of coingeckoTokensList) {
        const tokenResponse = await axios.get<CoingeckoTokenResponse>(
          `${tokenEndpointUrl}/${coingeckoToken.id}`
        );
        tokensList.map((token) => {
          if (
            token.id === tokenResponse.data.platforms.avalanche ||
            tokenResponse.data.links.blockchain_site.some(
              (site) => site && site.includes(token.id)
            )
          ) {
            const price = tokenResponse.data.market_data.current_price.usd;
            tokensListPrices.push({
              tokenId: token.id,
              price: typeof price === 'string' ? parseFloat(price) : price,
            });
          }
        });
      }

      return tokensListPrices;
    },
    {
      enabled: !!tokensList.length,
      refetchOnReconnect: false,
      refetchOnWindowFocus: false,
      retryDelay: 3000,
      retry: 3,
    }
  );
};
