import { Token } from '../../types';
import { useTokensList, usePoolsList } from '..';
import { useQuery } from 'react-query';

export const useDirectTokensForToken = (token?: Token) => {
  const { data: tokensList } = useTokensList();
  const { data: poolsList } = usePoolsList();

  return useQuery(
    ['direct-tokens-for-token', token?.id],
    () => {
      const poolsOfToken = poolsList!.filter((pool) =>
        pool.tokensList.find((poolToken) => poolToken.id === token!.id)
      );
      const directTokens: Token[] = [];

      poolsOfToken.map((pool) => {
        pool.tokensList.map((poolToken) => {
          if (
            !directTokens.find(
              (directToken) => directToken.id === poolToken.id
            ) &&
            poolToken.id !== token!.id
          ) {
            directTokens.push(
              tokensList!.find((_token) => _token.id === poolToken.id)!
            );
          }
        });
      });

      return directTokens;
    },
    {
      refetchOnReconnect: false,
      refetchOnWindowFocus: false,
      retryDelay: 3000,
      retry: 1,
      enabled: !!token && !!tokensList && !!poolsList,
    }
  );
};
