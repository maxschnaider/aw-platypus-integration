import { Token } from '../../types';
import { useTokensList, usePoolsList, useDirectTokensForToken } from '..';
import { useQuery } from 'react-query';

export const useMultihopTokensForToken = (token?: Token) => {
  const { data: tokensList } = useTokensList();
  const { data: poolsList } = usePoolsList();
  const { data: directTokens } = useDirectTokensForToken(token);

  return useQuery(
    ['multihop-tokens-for-token', token?.id],
    () => {
      const notPoolsOfToken = poolsList!.filter(
        (pool) =>
          !pool.tokensList.find((poolToken) => poolToken.id === token!.id)
      );
      const multihopTokens: Token[] = [];

      notPoolsOfToken.map((pool) => {
        directTokens!.map((directToken) => {
          if (pool.tokensList.find((_token) => _token.id === directToken.id)) {
            pool.tokensList.map((newMultihopToken) => {
              if (
                !directTokens!.find(
                  (directToken) => directToken.id === newMultihopToken.id
                ) &&
                !multihopTokens.find(
                  (multihopToken) => multihopToken.id === newMultihopToken.id
                ) &&
                newMultihopToken.id !== token!.id
              ) {
                multihopTokens.push(
                  tokensList!.find(
                    (_token) => _token.id === newMultihopToken.id
                  )!
                );
              }
            });
          }
        });
      });

      return multihopTokens;
    },
    {
      refetchOnReconnect: false,
      refetchOnWindowFocus: false,
      retryDelay: 3000,
      retry: 1,
      enabled: !!token && !!tokensList && !!poolsList && !!directTokens,
    }
  );
};
