import { Pool, SwapVariant, Token } from '../../types';
import { useQuery } from 'react-query';
import { useDirectTokensForToken } from '.';
import { usePoolsList } from '..';

export interface UseBuildSwapPathsResponse {
  tokenPath: Token[];
  poolPath: Pool[];
}

export interface UseBuildSwapPathsParams {
  fromToken?: Token;
  toToken?: Token;
}

export const useBuildSwapPaths = ({
  fromToken,
  toToken,
}: UseBuildSwapPathsParams) => {
  const { data: poolsList } = usePoolsList();
  const { data: fromTokenDirectTokens } = useDirectTokensForToken(fromToken);
  const { data: toTokenDirectTokens } = useDirectTokensForToken(toToken);

  return useQuery(
    ['build-swap-paths', fromToken?.id, toToken?.id],
    (): UseBuildSwapPathsResponse => {
      const poolsOfToken = (token: Token): Pool[] => {
        return poolsList!.filter((pool) =>
          pool.tokensList.find((poolToken) => poolToken.id === token.id)
        );
      };

      const fromPools = poolsOfToken(fromToken!);
      const toPools = poolsOfToken(toToken!);

      let middleToken: Token | undefined;

      const tokenPath = [fromToken!, toToken!];
      const poolPath: Pool[] = [];

      let swapVariant = SwapVariant.TokenToTokenMultihop;

      fromPools.map((pool) => {
        if (toPools.includes(pool) && poolPath.length === 0) {
          poolPath.push(pool);
          swapVariant = SwapVariant.TokenToTokenDirect;
        }
      });

      if (swapVariant === SwapVariant.TokenToTokenMultihop) {
        middleToken = fromTokenDirectTokens!.find((fromTokenDirectToken) =>
          toTokenDirectTokens!.find(
            (toTokenDirectToken) =>
              fromTokenDirectToken.id === toTokenDirectToken.id
          )
        );
        if (middleToken) {
          tokenPath.splice(1, 0, middleToken);

          let fromPool: Pool | undefined, toPool: Pool | undefined;
          const middlePools = poolsOfToken(middleToken);

          middlePools.map((pool) => {
            if (
              pool.tokensList.some(
                (poolToken) => poolToken.id === fromToken!.id
              )
            ) {
              console.log(pool);
              return (fromPool = pool);
            }
            if (
              pool.tokensList.some((poolToken) => poolToken.id === toToken!.id)
            ) {
              return (toPool = pool);
            }
          });
          if (fromPool && toPool) {
            poolPath.push(fromPool, toPool);
          }
        }
      }

      return {
        tokenPath: tokenPath,
        poolPath: poolPath,
      };
    },
    {
      refetchOnReconnect: false,
      refetchOnWindowFocus: false,
      retryDelay: 3000,
      retry: 1,
      enabled:
        !!fromToken &&
        !!toToken &&
        !!poolsList &&
        !!fromTokenDirectTokens &&
        !!toTokenDirectTokens,
    }
  );
};
