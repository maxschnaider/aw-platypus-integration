/* eslint-disable @typescript-eslint/no-unnecessary-type-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { useSigner } from 'wagmi';
import { useQuery } from 'react-query';
import { PLATYPUS_ROUTER_ADDRESS } from '../constants';
import { PlatypusRouter__factory } from '../contracts';

export const usePlatypusRouterContract = () => {
  const { data: signer } = useSigner();
  return useQuery(
    ['get-platypus-router-contract'],
    () => {
      return PlatypusRouter__factory.connect(PLATYPUS_ROUTER_ADDRESS, signer!);
    },
    {
      enabled: !!signer,
    }
  );
};
