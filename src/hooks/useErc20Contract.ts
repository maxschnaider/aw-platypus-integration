/* eslint-disable @typescript-eslint/no-unnecessary-type-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { ERC20__factory } from '../contracts';
import { useSigner } from 'wagmi';
import { useQuery } from 'react-query';

export const useErc20Contract = (address?: string | undefined) => {
  const { data: signer } = useSigner();
  return useQuery(
    ['get-erc20-contract', address],
    () => {
      return ERC20__factory.connect(address!, signer!);
    },
    {
      enabled: !!signer && !!address,
    }
  );
};
