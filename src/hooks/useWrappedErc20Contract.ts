/* eslint-disable @typescript-eslint/no-unnecessary-type-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { WrappedERC20__factory } from '../contracts';
import { useSigner } from 'wagmi';
import { useQuery } from 'react-query';

export const useWrappedErc20Contract = (address?: string | undefined) => {
  const { data: signer } = useSigner();
  return useQuery(
    ['get-wrapped-erc20-contract', address],
    () => {
      return WrappedERC20__factory.connect(address!, signer!);
    },
    {
      enabled: !!signer && !!address,
    }
  );
};
