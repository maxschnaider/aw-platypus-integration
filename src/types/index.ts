// import { ERC20, PlatypusPool } from '../contracts';

export interface Token {
  id: string;
  symbol: string;
  decimals: number;
  name?: string;
  price?: number;
  logoURI: string;
  logoFormat: 'png' | 'svg';
  isEth?: boolean;
  wrappedAddress?: string;
  // balance?: number;
  // contract?: ERC20;
}

export interface Pool {
  id: string;
  tokensList: {
    id: string;
    symbol: string;
  }[];
  // contract?: PlatypusPool;
}

export enum SwapSide {
  From,
  To,
}

export enum SwapVariant {
  Wrap, // AVAX to wAVAX
  Unwrap, // wAVAX to AVAX
  EthToToken, // AVAX to sAVAW
  TokenToEth, // sAVAX to AVAX
  TokenToTokenDirect, // ERC20 to ERC20 inside pool
  TokenToTokenMultihop, // ERC20 to ERC20 between pools
}

export enum SwapViewState {
  NoSigner,
  WrongNetwork,
  NoTokens,
  NoAmount,
  LoadingQuote,
  ErrorFetchingQuote,
  RequiresApprove,
  LoadingApprove,
  ReadyToSwap,
  LoadingSwap,
  SwapSubmitted,
}
